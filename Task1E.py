from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number
from tabulate import tabulate

def run():
    stations = build_station_list()
    river_station_number = rivers_by_station_number(stations, 9)
    print(tabulate(river_station_number, headers=['River Name','Number of Stations']))
    print('-'*60)

if __name__ == "__main__":
    print('-'*60)
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    print('-'*60)
    run()