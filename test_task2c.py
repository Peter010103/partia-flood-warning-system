"""Testing for Task 2C"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from tabulate import tabulate


def test_task2C():
    # Build list of stations
    stations = build_station_list()
    assert stations

    # Update latest level data for all stations
    update_water_levels(stations)

    N = 10

    N_highest_stations = stations_highest_rel_level(stations,N)
    assert N_highest_stations

    for i in range(len(N_highest_stations)-1):
        assert N_highest_stations[i].relative_water_level() >= N_highest_stations[i+1].relative_water_level()
    
    stations_tabulate = []
    for station in N_highest_stations:
        stations_tabulate.append((station.name, station.relative_water_level()))

    assert stations_tabulate
       