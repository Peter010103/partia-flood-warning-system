from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def run():

    # Build list of stations
    stations = build_station_list()

    inconsistentstations = sorted(inconsistent_typical_range_stations(stations))
    print("Stations with errors: ")
    for i in range(len(inconsistentstations)):
        print('   -',inconsistentstations[i])
    print('-'*60)

if __name__ == "__main__":
    print('-'*60)
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    print('-'*60)
    run()