from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from tabulate import tabulate


def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    N = 10

    N_highest_stations = stations_highest_rel_level(stations,N)
    #print (N_highest_stations)
    
    stations_tabulate = []
    for station in N_highest_stations:
        stations_tabulate.append((station.name, station.relative_water_level()))
       
    print(tabulate(stations_tabulate, headers=['Station Name','Relative Water Level']))
    print('-'*60)
if __name__ == "__main__":
    print('-'*60)
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    print('-'*60)
    run()
