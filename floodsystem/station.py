# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""

import numpy as np


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    def typical_range_consistent(self):
        
        if self.typical_range == None:
            return False
        else:
            lowrange = self.typical_range[0]
            highrange = self.typical_range[1]
             
            if lowrange > highrange:
                return False
            else:
                return True

    #Task 2B
    def relative_water_level(self):
        consistency = self.typical_range_consistent()

        if self.latest_level == None:
            return None

        elif consistency == False:
            return None

        else: 
            #relative_water_level = self.latest_level/self.typical_range[1]

            x = self.latest_level - self.typical_range[0]
            y = self.typical_range[1] - self.typical_range[0] 

            relative_water_level = x / y

        return relative_water_level
    
    'compute relative water level for history levels'
    def relative_history_levels(self):
        if self.typical_range_consistent() == True and self.latest_level != None and type(self.history_levels) == np.ndarray :
            ratio=(self.history_levels-self.typical_range[0])/(self.typical_range[1]-self.typical_range[0])
        else:
            ratio=None
        return ratio

"""Function that returns the list of inconsistent stations"""
def inconsistent_typical_range_stations(stations):
    inconsitentstations = []

    for station in stations:
        if station.typical_range_consistent() == False:
            inconsitentstations.append(station.name)

    return inconsitentstations