
"""This module contains a collection of functions related to
flood data.

"""

#For Task2B
""" function that returns a list of tuples(station(object) at which latest relative water level is over tol, relative water level) sorted by descending relative level"""
def stations_level_over_threshold(stations, tol):
    stations_level_over_threshold = []

    for station in stations:
        relative_water_level = station.relative_water_level()
        if relative_water_level == None:
            break

        elif relative_water_level > tol:
            stations_level_over_threshold.append((station,relative_water_level))
    
    sorted_stations_level_over_threshold = sorted(stations_level_over_threshold, key = lambda station: station[1], reverse = True)

    return sorted_stations_level_over_threshold  

#For Task2C
""" Function that returns a list of N stations at which water level, relative to typical range is highest""" 

def stations_highest_rel_level(stations,N):
    stations_highest_rel_level = []

    for station in stations:
        relative_water_level = station.relative_water_level()

        if relative_water_level == None:
            break
        else:
            stations_highest_rel_level.append((station,relative_water_level))

    sorted_stations_highest_rel_level = sorted(stations_highest_rel_level, key = lambda station: station[1], reverse = True)
    
    N_highest_stations = []
    for x in range(0,N):
        N_highest_stations.append(sorted_stations_highest_rel_level[x][0])

    return N_highest_stations
