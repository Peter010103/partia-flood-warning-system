"""This module contains a collection of functions related to plotting
"""
import matplotlib
import matplotlib.pyplot as plt
from floodsystem.analysis import polyfit
import matplotlib.dates
import datetime
import numpy as np
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels

# def plot_water_levels(station, dates, levels):
    
#     typical_low = []
#     typical_high = []
#     for n in range(len(dates)):
#         typical_low.append(station.typical_range[0]) 
#         typical_high.append(station.typical_range[1])

#     # Plot
#     plt.plot(dates, levels, 'g',dates,typical_high ,'r',dates,typical_low, 'b')

#     # Add axis labels, rotate date labels and add plot title
#     plt.xlabel('Date & Time')
#     plt.ylabel('Water level (m)')
#     plt.xticks(rotation=45)
#     plt.title(station.name)

#     # Display plot
#     plt.tight_layout()  # This makes sure plot does not cut off date labels

#     plt.show()

def plot_water_levels(N_highest_stations):

    N_highest_dict = {}
    for x in range(len(N_highest_stations)):
        station = N_highest_stations[x]       

        dt = 10
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))

        typical_low = []
        typical_high = []

        for n in range(len(dates)):
            typical_low.append(station.typical_range[0]) 
            typical_high.append(station.typical_range[1])

        N_highest_dict[x] = [dates,levels,typical_high,typical_low,station.name]

    # Plot
    plt.figure()

    for x in range(len(N_highest_stations)):
        plt.subplot(3,3,x+1)
        plt.plot(N_highest_dict[x][0], N_highest_dict[x][1],'g', label = 'water level')
        plt.plot(N_highest_dict[x][0],N_highest_dict[x][2],'r', label = 'typical high')
        plt.plot(N_highest_dict[x][0],N_highest_dict[x][3],'b', label = 'typical low')
        # Add axis labels, rotate date labels and add plot title
        plt.xlabel('Date & Time')
        plt.ylabel('Water level (m)')
        plt.xticks(rotation=80)
        plt.title(N_highest_dict[x][4])

        # plt.subplot(3,3,2)
        # plt.plot(N_highest_dict[1][0], N_highest_dict[1][1], 'g',N_highest_dict[1][0],N_highest_dict[1][2] ,'r',N_highest_dict[1][0],N_highest_dict[1][3], 'b')
        # # Add axis labels, rotate date labels and add plot title
        # plt.xlabel('Date & Time')
        # plt.ylabel('Water level (m)')
        # plt.xticks(rotation=45)
        # plt.title(N_highest_dict[1][4])

        # plt.subplot(3,3,3)
        # plt.plot(N_highest_dict[2][0], N_highest_dict[2][1], 'g',N_highest_dict[2][0],N_highest_dict[2][2] ,'r',N_highest_dict[2][0],N_highest_dict[2][3], 'b')
        # # Add axis labels, rotate date labels and add plot title
        # plt.xlabel('Date & Time')
        # plt.ylabel('Water level (m)')
        # plt.xticks(rotation=45)
        # plt.title(N_highest_dict[2][4])
    
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()  
    
''' Function that plots water level data and the best-fit polynomial '''
def plot_water_level_with_fit(station, dates, levels, p):

	if dates == []:
		print('{} station has no data to plot.'.format(station))
	else:
		t = dates
		level = levels
		f, d0 = polyfit(dates, levels, p)

		#range line
		stations=build_station_list()
		for station_ind in stations:
			if station_ind.name == station:
				typical_low = np.linspace(station_ind.typical_range[0],station_ind.typical_range[0],len(t))
				typical_high = np.linspace(station_ind.typical_range[1],station_ind.typical_range[1],len(t))

		# Plots
		plt.plot(t, level)
		plt.plot(t, f(np.array(matplotlib.dates.date2num(t))-d0))
		plt.plot(t, typical_low)
		plt.plot(t, typical_high)

		# Add axis labels, rotate date labels and add plot title
		plt.xlabel('date')
		plt.ylabel('water level (m)')
		plt.xticks(rotation=45);
		plt.title(station)

		# Display plot
		plt.tight_layout()  # This makes sure plot does not cut off date labels
		plt.show()