# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from haversine import haversine

# For task 1B
""" function that returns a list of tuples(station name, distance from p) sorted by distance"""
def stations_by_distance(stations, p):
    stations_by_distance = []
    for station in stations:
        #distance is given in km
        distance = haversine(station.coord, p)
        stations_by_distance.append((station.name, station.town, distance))
    return sorted_by_key(stations_by_distance, 2, 0)

# For task 1C
""" Function that creates a list of stations that are within radius r of the city centre"""
def stations_within_radius(stations, centre, r):
    stationlist = []
    for station in stations:
        #distance is given in km
        distance = haversine(station.coord, centre)
        if distance <= r:
            stationlist.append(station.name)
    stationlistsorted = sorted(stationlist)

    return stationlistsorted

# For task 1D
""" Function that returns a set of rivers with at least 1 monitoring station"""
def rivers_with_stations(stations):
    riverlist = []
    for station in stations:
        riverlist.append(station.river)
    riverset = set(riverlist)

    return riverset

""" Function that returns a dictionary mapping rivers to its monitoring stations"""
def stations_by_river(stations):
    rivertostation_dict = {}
    for station in stations:
        river = station.river
        if river in rivertostation_dict.keys():
            rivertostation_dict[river].append(station.name)
        else:
            rivertostation_dict[river] = [station.name]

    return rivertostation_dict

# For task 1E
"""" Function that returns a list of tuples of N number of (river name, number of stations)"""
def rivers_by_station_number(stations, N):
    rivertostation_dict = stations_by_river(stations)
    num_of_stations = []
    long_list = []
    short_list = []
    
    for river in rivertostation_dict:
        num_of_stations.append((river, len(rivertostation_dict[river])))
    
    long_list = sorted_by_key(num_of_stations, 1, 1)
    short_list = long_list[0:N]
    
    while long_list[N][1] == long_list[N-1][1]:
        short_list.append(long_list[N])
        N+=1
        
    return short_list