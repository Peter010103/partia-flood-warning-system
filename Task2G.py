import numpy as np
import datetime
from floodsystem.analysis import polyfit, risk
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level, stations_level_over_threshold
from floodsystem.datafetcher import fetch_measure_levels
import matplotlib.dates

def run(risk_level):
    low_risk, moderate_risk, high_risk, severe_risk = risk()

	#sort list of towns with warning
    towns_severe=[]
    towns_high=[]
    towns_moderate=[]
    towns_low=[]
    towns_counted=[]
    
    if risk_level == "severe":
    	print('****** SEVERE Risk ******')
    	for station in severe_risk:
    		if (station.town in towns_counted)==False:
    			towns_severe.append (station.town)
    			towns_counted.append (station.town)
    			print(station.town)
    	print(len(severe_risk), 'towns under severe risk')
    
    if risk_level == "high":
    	print('****** HIGH Risk ******')
    	for station in high_risk:
    		if (station.town in towns_counted)==False:
    			towns_high.append (station.town)
    			towns_counted.append (station.town)
    			print(station.town)
    	print(len(high_risk), 'towns under high risk')
        
    if risk_level == "moderate":
    	print('****** MODERATE Risk ******')
    	for station in moderate_risk:
    		if (station.town in towns_counted)==False:
    			towns_moderate.append (station.town)
    			towns_counted.append (station.town)
    			print(station.town)
    	print(len(moderate_risk), 'towns under moderate risk')
    
    if risk_level == "low":
    	print('****** LOW Risk ******')
    	for station in low_risk:
    		if (station.town in towns_counted)==False:
    			towns_low.append (station.town)
    			towns_counted.append (station.town)
    			print(station.town)
    	print(len(low_risk), 'towns under low risk')

if __name__ == "__main__":
    low_risk, moderate_risk, high_risk, severe_risk = risk()
    #print("There are {} towns under severe risk, {} towns under high risk, {} towns under moderate risk, {} towns under low risk." .format(len(severe_risk), len(high_risk), len(moderate_risk), len(low_risk)))
    #print("Which towns would you like to know?")
    risk_level = input("Level of risk: ")
    print("*** Peter and Daryl's Awesome Flood Warning System ***")
    run (risk_level)