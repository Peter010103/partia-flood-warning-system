import datetime
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    
    station_names = []
    station_data = []
    
    highest_stations = stations_highest_rel_level(stations, 5)
    
    for station in highest_stations:
        station_names.append(station.name)
        
    for name in station_names:
        for station in stations:
            if station.name == name:
                station_data.append(station)
                
    #for station in station_data:
    #    print(station.measure_id)
                
    for station in station_data:
        dt = 2
        dates, level = fetch_measure_levels(station.measure_id, dt = datetime.timedelta(days=dt))
        
        plot_water_level_with_fit(station.name, dates, level, 4)
    
    print('-'*60)

if __name__ == "__main__":
    print('-'*60)
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    print('-'*60)
    run()
