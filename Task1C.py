from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():

    # Build list of stations
    stations = build_station_list()

    # Coordinates of centre
    centre = (52.2053,0.1218)

    #radius boundary
    r = 10
    #prints the list of stations that are within the radius
    stationlistsorted = stations_within_radius(stations, centre, r)
    print("Stations within a 10km radius of Cambridge: ")
    for i in range(len(stationlistsorted)):
        print('   -', stationlistsorted[i])
    print('-'*60)

if __name__ == "__main__":
    print('-'*60)
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    print('-'*60)
    run()
