"""Testing for stations_by_distance function under geo module"""

# import station by distance
from floodsystem.geo import stations_by_distance

# import the station list
from floodsystem.stationdata import build_station_list

def test_distancesorter():

    #Builds a list of station
    stations = build_station_list()

    #coordinates of p
    p = (52.1984, 0.1207)
    
    #Builds a list of tuples
    station_distance = stations_by_distance(stations, p)

    #Assert station_distance is not empty
    assert station_distance
