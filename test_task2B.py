"""Testing for Task2B"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold
from tabulate import tabulate


def test_task2B():
    # Build list of stations
    stations = build_station_list()
    assert stations
    # Update latest level data for all stations
    update_water_levels(stations)

    tol = 0.8

    stations_over_threshold = stations_level_over_threshold(stations,tol)
    for i in range(len(stations_over_threshold)-1):
        assert stations_over_threshold[i][1] >= stations_over_threshold[i+1][1]
        assert stations_over_threshold[i][1] > tol