"""Testing for task1F"""

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def test_task1F():
    # Build list of stations
    stations = build_station_list()
    assert stations