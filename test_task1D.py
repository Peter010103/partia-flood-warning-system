"""Testing for task1D"""

from floodsystem.geo import rivers_with_stations 
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def test_task1D():
    # Build list of stations
    stations = build_station_list()
    assert stations

    #Gets the set of rivers with at least one station
    riverset = rivers_with_stations(stations)
    riverlist = sorted(list(riverset))
    assert riverlist
    
    #Second part of task 1D
    stationbyriver_dict = stations_by_river(stations)
    assert stationbyriver_dict

    #sort the list of stations by alphabetical order
    stations_of_aire = sorted(stationbyriver_dict['River Aire'])
    stations_of_cam = sorted(stationbyriver_dict['River Cam'])
    stations_of_thames = sorted(stationbyriver_dict['River Thames'])