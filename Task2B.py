
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold
from tabulate import tabulate


def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    tol = 0.8

    stations_over_threshold = stations_level_over_threshold(stations,tol)
    stations_tabulate = []
    for station in stations_over_threshold:
        stations_tabulate.append((station[0].name, station[1]))
       
    print(tabulate(stations_tabulate, headers=['Station Name','Relative Water Level']))
    print('-'*60)
if __name__ == "__main__":
    print('-'*60)
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    print('-'*60)
    run()
