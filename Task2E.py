
import matplotlib.pyplot as plt
import datetime

from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
#from floodsystem.plot import plot_water_levels
from floodsystem.plot import plot_water_levels


def run():

    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)
    
    N = 6
    
    N_highest_stations = stations_highest_rel_level(stations,N)
    print(N_highest_stations)
    
    # Fetch data over past 10 days
    # for station in N_highest_stations:

    #     dt = 10
    #     dates, levels = fetch_measure_levels(
    #         station.measure_id, dt=datetime.timedelta(days=dt))

    #     plot_water_levels(station,dates,levels)
    plot_water_levels(N_highest_stations)



if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
