# import station by distance
from floodsystem.geo import stations_by_distance

# import the station list
from floodsystem.stationdata import build_station_list

def run():
    #Builds a list of station
    stations = build_station_list()

    #coordinates of p
    p = (52.1984, 0.1207)

    station_distance = stations_by_distance(stations, p)
    print("The top 10 closest river stations from Cambridge: ")
    for i in range(10):
        print ('   -', station_distance[i])
    print('\n')
    print("The top 10 furthest river stations from Cambridge: ")
    for i in range(-10, 0):
        print ('   -', station_distance[i])
    print('-'*60)

if __name__ == "__main__":
    print('-'*60)
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    print('-'*60)
    run()