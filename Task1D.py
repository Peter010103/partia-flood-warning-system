from floodsystem.geo import rivers_with_stations 
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def run():

    # Build list of stations
    stations = build_station_list()

    #Gets the set of rivers with at least one station
    riverset = rivers_with_stations(stations)
    riverlist = sorted(list(riverset))
    print ("Number of rivers with at least 1 station: ",'\n', '   ', len(riverlist))
    print ("The first 10 rivers with greatest number of stations: ")
    for i in range(10):
        print('   -', riverlist[i])
    print('-'*60)

    #Second part of task 1D
    stationbyriver_dict = stations_by_river(stations)

    #sort the list of stations by alphabetical order
    stations_of_aire = sorted(stationbyriver_dict['River Aire'])
    stations_of_cam = sorted(stationbyriver_dict['River Cam'])
    stations_of_thames = sorted(stationbyriver_dict['River Thames'])

    print("Stations on the Aire: ")
    for i in range(len(stations_of_aire)):
        print('   -', stations_of_aire[i])
    print('\n')
    print("Stations on the Cam: ")
    for i in range(len(stations_of_cam)):
        print('   -', stations_of_cam[i])
    print('\n')
    print("Stations on the Thames: ")
    for i in range(len(stations_of_thames)):
        print('   -', stations_of_thames[i])
        
    print('-'*60)

if __name__ == "__main__":
    print('-'*60)
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    print('-'*60)
    run()
